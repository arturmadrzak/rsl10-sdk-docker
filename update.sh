#!/bin/sh
# rsl10-sdk-docker project (https://gitlab.com/arturmadrzak/rsl10-sdk-docker)
# Copyright (c) 2020 Artur Mądrzak <artur@madrzak.eu>

set -eu

TARGET="./sdk"

echo_err()
{
	>&2 echo "ERROR: ${*}"
}

usage() 
{
    echo "Usage: ${0} [OPTIONS] RSL10_SOFTWARE_PACKAGE.ZIP"
    echo ""
    echo "Extracts CMSIS pack from zip file provided by ONSemiconductor to sdk directory"
}

extract()
{
    _pack=$(unzip -j -l "${1}" \
           RSL10_Software_Package/ONSemiconductor.RSL10.*.pack | \
           sed -n 's/^.*\(ONSemiconductor\.RSL10\..*\.pack\)/\1/p')

    if ! unzip -qq -u -j -d "${TARGET}" "${1}" "RSL10_Software_Package/${_pack}"; then 
        echo_err "Failed to extract ${1}"
        exit 1
    fi
    
    _version=${_pack#ONSemiconductor.RSL10.}
    VERSION=${_version%.pack}
}

commit()
{
    _version="${1:?Missing version}"
    git add "${TARGET}/ONSemiconductor.RSL10.${_version}.pack"
    git status
    git commit --message "Add SDK: ${_version} from vendor zip"
}

tag()
{
    git tag --annotate --sign --message "Release SDK: ${VERSION}" "v${VERSION}"
}

while getopts ":hr:" _options; do
    case "${_options}" in
    h)
        usage
        exit 0
        ;;
    :)
        echo_err "Option -${OPTARG} requires an argument."
        exit 1
        ;;
    ?)
        echo_err "Invalid option: -${OPTARG}"
        exit 1
        ;;
    esac
done
shift "$((OPTIND - 1))"

_package=${1:?Missing software package}
if [ ! -f "${_package}" ]; then
    echo_err "File ${_package} not found!"
    exit 1
fi

extract "${_package}"
commit "${VERSION}"

echo -n "Do you wish to create a tag for release (y/n)?"
while read -r yn; do
    case $yn in
        [Yy]* ) tag; break;;
        [Nn]* ) exit;;
        * ) echo -n "Please answer (y)es or (n)o.";;
    esac
done


exit 0
